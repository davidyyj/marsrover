﻿using System;
using MarsRover;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mars Rover:\r\n");
            var input = "5 5\r\n";
            input += "1 2 N\r\n";
            input += "LMLMLMLMM\r\n";
            input += "3 3 E\r\n";
            input += "MMRMMRMRRM";

            Console.WriteLine($"Input:\r\n{input}\r\n");
            var manager = new RoverManager(new RoverFactory());
            var output = manager.HandleExploreRequest(input);
            Console.WriteLine($"Output:\r\n{output}\r\n");

            Console.ReadLine();
        }
    }
}
