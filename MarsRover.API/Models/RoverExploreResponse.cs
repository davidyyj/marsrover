﻿namespace MarsRover.API.Models
{
    public class RoverExploreResponse
    {
        public string[] Output { get; set; }
    }
}
