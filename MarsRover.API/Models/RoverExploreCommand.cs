﻿using MediatR;

namespace MarsRover.API.Models
{
    public class RoverExploreCommand : IRequest<RoverExploreResponse>
    {
        public string[] Inputs { get; set; }
    }
}
