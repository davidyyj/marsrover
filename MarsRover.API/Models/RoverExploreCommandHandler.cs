﻿using System.Threading;
using System.Threading.Tasks;
using MarsRover.Interfaces;
using MediatR;

namespace MarsRover.API.Models
{
    public class RoverExploreCommandHandler : IRequestHandler<RoverExploreCommand, RoverExploreResponse>
    {
        private readonly IRoverManager _roverManager;

        public RoverExploreCommandHandler(IRoverManager roverManager)
        {
            _roverManager = roverManager;
        }

        public async Task<RoverExploreResponse> Handle(RoverExploreCommand command, CancellationToken cancellationToken = default)
        {
            var commandInputs = string.Join(MarsRoverConsts.LineBreak, command.Inputs);

            var output = await Task.FromResult(_roverManager.HandleExploreRequest(commandInputs));            

            return new RoverExploreResponse() { Output = output.Split(MarsRoverConsts.LineBreak)};
        }
    }
}
