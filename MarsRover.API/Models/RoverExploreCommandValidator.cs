﻿using System.Linq;
using FluentValidation;

namespace MarsRover.API.Models
{
    public class RoverExploreCommandValidator : AbstractValidator<RoverExploreCommand>
    {
        public RoverExploreCommandValidator()
        {
            RuleFor(request => request.Inputs)
                .NotNull()
                .Must(input => input.Length >= 3);
        }
    }
}
