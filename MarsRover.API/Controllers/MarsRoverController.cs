﻿using System.Threading.Tasks;
using MarsRover.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace MarsRover.API.Controllers
{
    public class MarsRoverController : ApiControllerBase
    {
        public MarsRoverController()
        {
        }

        [HttpPost]
        [Route("explore-rovers")]
        public async Task<ActionResult<RoverExploreResponse>> ExploreRoversAsync([FromBody] RoverExploreCommand request)
        {
            return await Mediator.Send(request);
        }
    }
}
