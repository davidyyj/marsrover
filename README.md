### Development ###

* Visual Studio 2019
* Asp.Net 5.0

API Request:
{
  "inputs": [
   "5 5",
   "1 2 N",
   "LMLMLMLMM",
   "3 3 E",
   "MMRMMRMRRM"
  ]
}

API Response:
{
  "output": [
    "1 3 N",
    "5 1 E"
  ]
}