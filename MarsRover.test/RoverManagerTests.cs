﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using MarsRover.Exceptions;
using MarsRover.Interfaces;
using Moq;
using NUnit.Framework;

namespace MarsRover.test
{
    [TestFixture]
    public class RoverManagerTests
    {
        private readonly List<string> _instructionNameList = Enum.GetNames(typeof(Instructions)).ToList();

        [Test]
        public void Handle_ValidReqeust_Pass()
        {
            var instructionsString = "LMLMLMLMM";
            var roverMock = new Mock<IRover>();
            var roverFactoryMock = new Mock<IRoverFactory>();
            var requestInputs = string.Join(MarsRoverConsts.LineBreak, new string[] { "5 5", "1 2 N", instructionsString });
            var responseExpected = string.Join(MarsRoverConsts.LineBreak, new string[] { "1 3 N" });

            var plateau = new Plateau()
            {
                MinX = 0,
                MinY = 0,
                MaxX = 5,
                MaxY = 5
            };

            var position = new RoverPosition
            (
                new Coordinates(1, 2),
                Orientations.N
            );
            
            roverMock.Setup(x => x.DeployRover(plateau, position));

            var instructionNames = instructionsString.ToCharArray();

            List<Instructions> instructions = new();
       
            foreach (var instruction in instructionNames)
            {
                instructions.Add((Instructions)_instructionNameList.IndexOf(instruction.ToString()));
            }

            roverMock.Setup(x => x.ProcessInstructions(instructions.ToArray()));

            var positionResult = new RoverPosition
            (
                new Coordinates(1, 3),
                Orientations.N
            );

            roverMock.Setup(x => x.CurrentRoverPosition).Returns(positionResult);
            roverFactoryMock.Setup(x => x.BuildRover()).Returns(roverMock.Object);

            var roverManager = new RoverManager(roverFactoryMock.Object);
            var response = roverManager.HandleExploreRequest(requestInputs);
                        
            response.Should().Be(responseExpected);
        }

        [Test]
        public void Handle_InvalidPlateau_ReturnErrorMessage()
        {
            var instructionsString = "LMLMLMLMM";
            var roverFactoryMock = new Mock<IRoverFactory>();
            var requestInputs = string.Join(MarsRoverConsts.LineBreak, new string[] { "A B", "1 2 N", instructionsString });
            
            var roverManager = new RoverManager(roverFactoryMock.Object);
            var response = roverManager.HandleExploreRequest(requestInputs);

            response.Should().Contain(InvalidRequestException.ErrorMessage);
        }

        [Test]
        public void Handle_InvalidRoverOrientation_ReturnErrorMessage()
        {
            var instructionsString = "LMLMLMLMM";
            var roverFactoryMock = new Mock<IRoverFactory>();
            var requestInputs = string.Join(MarsRoverConsts.LineBreak, new string[] { "5 5", "1 2 1", instructionsString });

            var roverManager = new RoverManager(roverFactoryMock.Object);
            var response = roverManager.HandleExploreRequest(requestInputs);

            response.Should().Contain(InvalidRequestException.ErrorMessage);
        }

        [Test]
        public void Handle_InvalidRoverCoordinates_ReturnErrorMessage()
        {
            var instructionsString = "LMLMLMLMM";
            var roverFactoryMock = new Mock<IRoverFactory>();
            var requestInputs = string.Join(MarsRoverConsts.LineBreak, new string[] { "5 5", "A B N", instructionsString });

            var roverManager = new RoverManager(roverFactoryMock.Object);
            var response = roverManager.HandleExploreRequest(requestInputs);

            response.Should().Contain(InvalidRequestException.ErrorMessage);
        }

        [Test]
        public void Handle_InvalidRoverInstruction_ReturnErrorMessage()
        {
            var instructionsString = "SMLMLMLMM";
            var roverFactoryMock = new Mock<IRoverFactory>();
            var requestInputs = string.Join(MarsRoverConsts.LineBreak, new string[] { "5 5", "1 2 N", instructionsString });

            var roverManager = new RoverManager(roverFactoryMock.Object);
            var response = roverManager.HandleExploreRequest(requestInputs);

            response.Should().Contain(InvalidRequestException.ErrorMessage);
        }
    }
}
