﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace MarsRover.test
{
    [TestFixture]
    public class RoverTests
    {
        private readonly List<string> _instructionNameList = Enum.GetNames(typeof(Instructions)).ToList();

        [Test]
        public void DeployRover_RoverPosition_Same()
        {
            var plateau = new Plateau()
            {
                MinX = 0,
                MinY = 0,
                MaxX = 5,
                MaxY = 5
            };

            var position = new RoverPosition
            (
                new Coordinates(1, 2),
                Orientations.N
            );

            var rover = new Rover();
            rover.DeployRover(plateau, position);

            comparePosition(rover.CurrentRoverPosition, position);
        }

        [Test]
        public void ProcessInstructions_RoverPosition_Expected()
        {
            var instructionsString = "LMLMLMLMM";
            
            var plateau = new Plateau()
            {
                MinX = 0,
                MinY = 0,
                MaxX = 5,
                MaxY = 5
            };

            var position = new RoverPosition
            (
                new Coordinates(1, 2),
                Orientations.N
            );

            var rover = new Rover();
            rover.DeployRover(plateau, position);

            var instructionNames = instructionsString.ToCharArray();

            List<Instructions> instructions = new();

            foreach (var instruction in instructionNames)
            {
                instructions.Add((Instructions)_instructionNameList.IndexOf(instruction.ToString()));
            }

            rover.ProcessInstructions(instructions.ToArray());

            var positionExpected = new RoverPosition
            (
                new Coordinates(1, 3),
                Orientations.N
            );

            comparePosition(rover.CurrentRoverPosition, positionExpected);
        }

        private void comparePosition(RoverPosition position, RoverPosition positionExpected)
        {
            position.CurrentCoordinates.X.Should()
                .Be(positionExpected.CurrentCoordinates.X);

            position.CurrentCoordinates.Y.Should()
                .Be(positionExpected.CurrentCoordinates.Y);

            position.CurrentOrientation.Should()
                .Be(positionExpected.CurrentOrientation);
        }
    }
}
