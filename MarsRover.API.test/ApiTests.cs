﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MarsRover.API.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using NUnit.Framework;

namespace MarsRover.API.test
{
    [TestFixture]
    public class ApiTests
    {
        const string ApiUrl = "http://localhost:5000/api/MarsRover/explore-rovers";

        private readonly TestServer _server;
        private readonly HttpClient _client;

        public ApiTests()
        {
            // Arrange
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        private async Task<string> postRequet(object request)
        {
            var json = JsonConvert.SerializeObject(request);

            var buffer = Encoding.UTF8.GetBytes(json);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _client.PostAsync(ApiUrl, byteContent);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            return responseString;
        }

        private async Task runTestCase(RoverExploreCommand request, RoverExploreResponse expectResponse)
        {
            var responseJson = await postRequet(request);

            RoverExploreResponse response = JsonConvert.DeserializeObject<RoverExploreResponse>(responseJson);

            response.Output.SequenceEqual(expectResponse.Output).Should().BeTrue();
        }

        [Test]
        public async Task RoverRequest_2Rovers_Return2Response()
        {
            var request = new RoverExploreCommand() { Inputs = new string[] { "5 5","1 2 N","LMLMLMLMM","3 3 E","MMRMMRMRRM" } };
            var expectedResult = new RoverExploreResponse()
            {
                Output = new string[] { "1 3 N", "5 1 E" }
            };
            
            await runTestCase(request, expectedResult);            
        }
    }
}
