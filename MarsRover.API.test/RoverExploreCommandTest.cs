﻿using System.Threading.Tasks;
using FluentAssertions;
using MarsRover.API.Models;
using MarsRover.Interfaces;
using Moq;
using NUnit.Framework;

namespace MarsRover.API.test
{
    [TestFixture]
    public class RoverExploreCommandTest
    {
        [Test]
        public async Task Handle_ValidReqeust_Pass()
        {            
            var roverManagerMock = new Mock<IRoverManager>();
            var command = new RoverExploreCommand() { Inputs = new string[] { "5 5", "1 2 N", "LMLMLMLMM", "3 3 E", "MMRMMRMRRM" } };
            var responseExpected = new RoverExploreResponse()
            {
                Output = new string[] { "1 3 N", "5 1 E" }
            };

            roverManagerMock.Setup(x => x.HandleExploreRequest(string.Join(
                MarsRoverConsts.LineBreak, command.Inputs))).Returns(string.Join(MarsRoverConsts.LineBreak,responseExpected.Output));
            
            var handler = new RoverExploreCommandHandler(roverManagerMock.Object);
            var response = await handler.Handle(command);
                        
            response.Should().BeEquivalentTo(responseExpected);
        }
    }
}
