﻿using FluentAssertions;
using MarsRover.API.Models;
using NUnit.Framework;

namespace MarsRover.API.test
{
    [TestFixture]
    public class RoverExploreCommandValidatorTests
    {
        [Test]
        public void InputeLineLessThanMinLines_Validator_False()
        {
            var validator = new RoverExploreCommandValidator();
            var request = new RoverExploreCommand() { Inputs = new string[] { "5 5"} };
            validator.Validate(request).IsValid.Should().BeFalse();
        }

        [Test]
        public void InputeLineMoreThanMinLines_Validator_False()
        {
            var validator = new RoverExploreCommandValidator();
            var request = new RoverExploreCommand() { Inputs = new string[] { "5 5", "1 2 N", "LMLMLMLMM"} };
            validator.Validate(request).IsValid.Should().BeTrue();
        }        
    }
}
