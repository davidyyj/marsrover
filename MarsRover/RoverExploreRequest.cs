﻿namespace MarsRover
{
    public class RoverExploreRequest
    {
        public RoverPosition DeployPosition { get; set; }
        public Instructions[] ExploreInstructions { get; set; }
    }
}
