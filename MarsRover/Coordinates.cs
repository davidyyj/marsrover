﻿namespace MarsRover
{
    public class Coordinates
    {
        public int X { get; set; }

        public int Y { get; set; }

        public Coordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Coordinates operator +(Coordinates a, Coordinates b)
        => new Coordinates(a.X + b.X, a.Y + b.Y);
    }
}
