﻿using System;

namespace MarsRover.Exceptions
{
    public class InvalidRequestException : Exception
    {
        public const string ErrorMessage = "Invalid Request: ";
        public InvalidRequestException()
            : base()
        {
        }

        public InvalidRequestException(string message)
            : base(ErrorMessage + message)
        {
        }

        public InvalidRequestException(string message, Exception innerException)
            : base(ErrorMessage + message, innerException)
        {
        }
    }
}
