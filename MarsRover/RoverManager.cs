﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MarsRover.Exceptions;
using MarsRover.Interfaces;

namespace MarsRover
{
    public class RoverManager : IRoverManager
    {
        private readonly List<string> _orientationNameList = Enum.GetNames(typeof(Orientations)).ToList();
        private readonly List<string> _instructionNameList = Enum.GetNames(typeof(Instructions)).ToList();
        private readonly IRoverFactory _roverFactory;

        public RoverManager(IRoverFactory factory)
        {
            _roverFactory = factory;
        }

        public string HandleExploreRequest(string request)
        {
            var stringReader = new StringReader(request);

            try
            {
                var plateau = parsePlateau(stringReader);
                var roverExploreRequestList = parseRequests(stringReader);
                return processRequests(plateau, roverExploreRequestList);
            }
            catch (InvalidRequestException e)
            {
                return e.Message;
            }
            catch (Exception)
            {
                // Invalid Enum value.
                return InvalidRequestException.ErrorMessage;
            }            
        }

        private Plateau parsePlateau(StringReader stringReader)
        {
            var plateauSetting = stringReader.ReadLine();

            try
            {
                return buildPlateau(plateauSetting);
            }
            catch (Exception)
            {
                throw new InvalidRequestException($"Invalid plateau {plateauSetting}");
            }
        }

        private List<RoverExploreRequest> parseRequests(StringReader stringReader)
        {
            List<RoverExploreRequest> roverExploreRequestList = new();

            while (true)
            {
                var roverPosition = stringReader.ReadLine();
                if (roverPosition == null)
                {
                    break;
                }

                var roverInstructions = stringReader.ReadLine();
                if (roverPosition == null)
                {
                    break;
                }

                try
                {
                    var roverExploreRequest = buildRoverExploreRequest(roverPosition, roverInstructions);
                    roverExploreRequestList.Add(roverExploreRequest);
                }
                catch (Exception)
                {
                    throw new InvalidRequestException($"Invalid input {roverPosition},{roverInstructions}");
                }
            }

            return roverExploreRequestList;
        }

        private string processRequests(Plateau plateau, List<RoverExploreRequest> roverExploreRequestList)
        { 
            List<string> output = new();

            foreach (var exploreRequest in roverExploreRequestList)
            {
                var rover = _roverFactory.BuildRover();

                rover.DeployRover(plateau, exploreRequest.DeployPosition);
                rover.ProcessInstructions(exploreRequest.ExploreInstructions);

                output.Add(buildRoverPositionOutput(rover.CurrentRoverPosition));
            }

            return string.Join(MarsRoverConsts.LineBreak, output);
        }

        private string buildRoverPositionOutput(RoverPosition rover)
        {
            return  $"{rover.CurrentCoordinates.X}{MarsRoverConsts.StringItemBreak}" +
                    $"{rover.CurrentCoordinates.Y}{MarsRoverConsts.StringItemBreak}" +
                    $"{Enum.GetName(typeof(Orientations), rover.CurrentOrientation)}"; 
        }


        private Plateau buildPlateau(string plateauSetting)
        {
            var stringArray = plateauSetting.Split(MarsRoverConsts.StringItemBreak);

            var arrayIndex = 0;
            var x = Convert.ToInt32(stringArray[arrayIndex++]);
            var y = Convert.ToInt32(stringArray[arrayIndex]);

            if(x < 0 || y < 0)
            {
                throw new InvalidRequestException($"Invalid plateau {plateauSetting}");
            }

            return new Plateau()
            {
                MinX = 0,
                MinY = 0,
                MaxX = x,
                MaxY = y
            };
        }

        private RoverExploreRequest buildRoverExploreRequest(string roverPosistion, string roverInstructions)
        {
            var roverProperties = roverPosistion.Split(MarsRoverConsts.StringItemBreak);

            var roverPropertyIndex = 0;
            var x = Convert.ToInt32(roverProperties[roverPropertyIndex++]);
            var y = Convert.ToInt32(roverProperties[roverPropertyIndex++]);
            var orientation = (Orientations)_orientationNameList.IndexOf(roverProperties[roverPropertyIndex]);

            var instructionNames = roverInstructions.ToCharArray();

            List<Instructions> instructions = new();
            instructions.AddRange
            (
                from instruction 
                in instructionNames
                select (Instructions)_instructionNameList.IndexOf(instruction.ToString())
            );
            
            RoverExploreRequest request = new()
            {
                DeployPosition = new RoverPosition
                (
                    new Coordinates(x, y),
                    orientation
                ),
                ExploreInstructions = instructions.ToArray()
            };

            return request;
        }
    }
}
