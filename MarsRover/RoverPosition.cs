﻿namespace MarsRover
{
    public class RoverPosition
    {
        public Coordinates CurrentCoordinates { get; set; }      

        public Orientations CurrentOrientation { get; set; }  
        
        public RoverPosition(Coordinates currentCoordinates, Orientations currentOrientation)
        {
            CurrentCoordinates = currentCoordinates;
            CurrentOrientation = currentOrientation;
        }
    }
}
