﻿namespace MarsRover
{
    public class InstructionResult
    {
        public Coordinates CoordinatesMove { get; private set; }

        public Orientations OrientationTurnLeft { get; private set; }

        public Orientations OrientationTurnRight { get; private set; }

        public InstructionResult
        (
            Coordinates coordinatesMove,
            Orientations orientationTurnLeft,
            Orientations orientationTurnRight
        )
        {
            CoordinatesMove = coordinatesMove;
            OrientationTurnLeft = orientationTurnLeft;
            OrientationTurnRight = orientationTurnRight;
        }
    }
}
