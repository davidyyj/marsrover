﻿using System;
using System.Collections.Generic;
using MarsRover.Exceptions;
using MarsRover.Interfaces;

namespace MarsRover
{
    public class Rover : IRover
    {
        private readonly Dictionary<Orientations, InstructionResult> _instructionResultMap = new();
        
        private Plateau _plateau;

        private bool _depoloyed = false;

        public RoverPosition CurrentRoverPosition { get; private set; }                 

        public Rover()
        {            
            _instructionResultMap.Add
            (
                Orientations.N, 
                new InstructionResult
                (
                    new Coordinates(0, 1),
                    Orientations.W,
                    Orientations.E
                )
            );

            _instructionResultMap.Add
            (
                Orientations.E,
                new InstructionResult
                (
                    new Coordinates(1, 0),
                    Orientations.N,
                    Orientations.S
                )
            );

            _instructionResultMap.Add
            (
                Orientations.S,
                new InstructionResult
                (
                    new Coordinates(0, -1),
                    Orientations.E,
                    Orientations.W
                )
            );

            _instructionResultMap.Add
            (
                Orientations.W,
                new InstructionResult
                (
                    new Coordinates(-1, 0),
                    Orientations.S,
                    Orientations.N
                )
            );
        }

        public void DeployRover
        (
            Plateau plateau,
            RoverPosition position
        )
        {
            _plateau = plateau;
            CurrentRoverPosition = position;            
            _depoloyed = plateau.IsValidCoordinates(position.CurrentCoordinates);

            if (!_depoloyed)
            {
                throw new InvalidRequestException("Rover Deploy Error"); ;
            }
        }

        public void ProcessInstructions(Instructions[] instructions)
        {            
            if(!_depoloyed)
            {
                throw new InvalidRequestException("Rover Deploy Error"); ;
            }

            foreach (var instruction in instructions)
            {
                var instructionResult = _instructionResultMap[CurrentRoverPosition.CurrentOrientation];
                switch (instruction)
                {
                    case Instructions.L:
                        {
                            CurrentRoverPosition.CurrentOrientation = instructionResult.OrientationTurnLeft;
                        }
                        break;

                    case Instructions.R:
                        {
                            CurrentRoverPosition.CurrentOrientation = instructionResult.OrientationTurnRight;
                        }
                        break;

                    case Instructions.M:
                        {
                            var coordinates = CurrentRoverPosition.CurrentCoordinates + instructionResult.CoordinatesMove;
                            if (!_plateau.IsValidCoordinates(coordinates))
                            {
                                throw new InvalidRequestException("Rover Instruction Error");
                            }

                            CurrentRoverPosition.CurrentCoordinates = coordinates;
                        }
                        break;

                    default:
                        throw new NotImplementedException();
                }
            }
        }
    }
}
