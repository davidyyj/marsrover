﻿using MarsRover.Interfaces;

namespace MarsRover
{
    public class RoverFactory : IRoverFactory
    {
        public IRover BuildRover()
        {
            return new Rover();
        }
    }
}
