﻿namespace MarsRover.Interfaces
{
    public interface IRoverFactory
    {
        IRover BuildRover();
    }
}
