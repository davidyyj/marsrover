﻿namespace MarsRover.Interfaces
{
    public interface IRoverManager
    {
        string HandleExploreRequest(string request);
    }
}
