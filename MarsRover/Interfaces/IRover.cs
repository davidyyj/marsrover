﻿namespace MarsRover.Interfaces
{
    public interface IRover
    {
        RoverPosition CurrentRoverPosition { get; }

        void DeployRover
        (
            Plateau plateau,
            RoverPosition position
        );

        void ProcessInstructions(Instructions[] instructions);
    }
}
