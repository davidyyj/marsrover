﻿namespace MarsRover
{
    public class Plateau
    {
        public int MinX { get; set; }

        public int MinY { get; set; }

        public int MaxX { get; set; }

        public int MaxY { get; set; }

        public bool IsValidCoordinates(Coordinates coordinates)
        {
            return (coordinates.X >= MinX) &&
                   (coordinates.X <= MaxX) &&
                   (coordinates.Y >= MinY) &&
                   (coordinates.Y <= MaxY);
        }
    }
}
